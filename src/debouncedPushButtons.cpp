/* jlf 20220110 */
#include "debouncedPushButtons.h"
// #include "scanPushButtons.h"

Debounced_PushButton::Debounced_PushButton(uint8_t pin, uint8_t inputMode = INPUT_PULLUP)
{
  m_pin = pin;
  pinMode(pin, inputMode);
  setCountMode(NO_COUNT);
  Serial.println("Create PushButton");
}
Debounced_PushButton::Debounced_PushButton(uint8_t pin, enum_CountMode mode, uint8_t inputMode = INPUT_PULLUP)
{
  m_pin = pin;
  pinMode(pin, inputMode);
  setCountMode(mode);
  Serial.println("Create PushButton");
}
Debounced_PushButton::~Debounced_PushButton()
{
  //
}
void Debounced_PushButton::setState(enum_State state)
{
  switch (state)
  {
  case DOWN:
    bitClear(m_flag_State, 0); // bit 0 set to zero
    bitSet(m_flag_State, 1);   // bit 1 set to 1 (just changed)
    if (bitRead(m_flag_State, 3) == 1)
    {
      incCount();
    }
    break;
  case UP:
    bitSet(m_flag_State, 0); // bit 0 set to 0
    bitSet(m_flag_State, 1); // bit 1 set to 1 (just changed)
    if (bitRead(m_flag_State, 2) == 1)
    {
      incCount();
    }
    break;
  default:
    // Nothing to do here
    break;
  }
}
void Debounced_PushButton::setCountMode(enum_CountMode mode)
{
  // m_countMode = mode;
  if ((mode == RISING_COUNT) || (mode == BOTH_COUNT))
  {
    bitSet(m_flag_State, 2);
  }
  else
  {
    bitClear(m_flag_State, 2);
  }
  if ((mode == FALLING_COUNT) || (mode == BOTH_COUNT))
  {
    bitSet(m_flag_State, 3);
  }
  else
  {
    bitClear(m_flag_State, 3);
  }
  if (mode == NO_COUNT)
  {
    bitClear(m_flag_State, 2);
    bitClear(m_flag_State, 3);
  }
  resetCount();
}
enum_State Debounced_PushButton::getState()
{
  if (bitRead(m_flag_State, 0) == 0)
  {
    return UP;
  }
  else
  {
    return DOWN;
  }
}

bool Debounced_PushButton::hasJustChanged()
{
  bool changed = (bitRead(m_flag_State, 1) != 0);
  bitClear(m_flag_State, 1); // We reset the JustChanged bit to 0
  return changed;
}

bool Debounced_PushButton::isPressed()
{
  return (bitRead(m_flag_State, 0) != 0);
}
bool Debounced_PushButton::isReleased()
{
  return (bitRead(m_flag_State, 0) == 0);
}
void Debounced_PushButton::setLastChangeTime(uint32_t time)
{
  m_lastChangeTime = time;
}

uint32_t Debounced_PushButton::getLastChangeTime()
{
  return m_lastChangeTime;
}

void Debounced_PushButton::incCount()
{
  m_count++;
}
uint32_t Debounced_PushButton::getCount()
{
  return m_count;
}
void Debounced_PushButton::resetCount()
{
  m_count = 0;
}

uint8_t Debounced_PushButton::getPin()
{
  return m_pin;
}





// 