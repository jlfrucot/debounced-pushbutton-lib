#ifndef SCANPUSHBUTTONS_H
#define SCANPUSHBUTTONS_H

#include "scanPushButtons.h"

Scan_PushButtons::Scan_PushButtons(uint16_t stackDepth, UBaseType_t priority, const char *name) : Thread{stackDepth, priority, name}

{
  evt_buttonsState = xEventGroupCreate();
  xEventGroupClearBits(evt_buttonsState, 0xFFFFFF);
  evt_buttonsToggled = xEventGroupCreate();
  xEventGroupClearBits(evt_buttonsToggled, 0xFFFFFF);
}

Scan_PushButtons::~Scan_PushButtons()
{
}
void Scan_PushButtons::setTaskDelay(uint16_t delay)
{
  m_taskDelay = delay;
}
void Scan_PushButtons::addPushButton(Debounced_PushButton *button)
{
  if (m_nb_registeredButtons < m_nb_max)
  {
    m_tab_pushButtons[m_nb_registeredButtons] = button;
    m_nb_registeredButtons++;
  }
}
Debounced_PushButton **Scan_PushButtons::getPushButtonsArray()
{
  return m_tab_pushButtons;
}

uint8_t Scan_PushButtons::getPushButtonsArraySize()
{
  return m_nb_registeredButtons;
}
void Scan_PushButtons::startUpdatePushButtons()
{

  new Update_PushButtons(1024, 2, "update buttons", m_tab_pushButtons, m_nb_registeredButtons, evt_buttonsToggled, evt_buttonsState);
}

void Scan_PushButtons::stop()
{
  vTaskSuspend(taskHandle);
}
void Scan_PushButtons::restart()
{
  vTaskResume(taskHandle);
}
inline uint32_t Scan_PushButtons::debounce(uint32_t sample, uint32_t *toggle)
{
  // use a vertical Counter to debounce switches
  static uint32_t state, cnt0, cnt1;
  uint32_t delta;

  delta = sample ^ state;
  cnt1 = (cnt1 ^ cnt0) & delta;
  cnt0 = ~cnt0 & delta;

  *toggle = delta & ~(cnt0 | cnt1);
  state ^= *toggle;

  return state;
}

void Scan_PushButtons::Main()
{
  while (true)
  {
    // First, we test the different pins whose number is in tab_BtnPins
    m_flag_ButtonsState = 0;
    for (uint8_t i = 0; i < m_nb_registeredButtons; i++)
    {
      if (digitalRead(m_tab_pushButtons[i]->getPin()) == 0)
      {
        m_flag_ButtonsState = (m_flag_ButtonsState | (1u << i));
      }
    }
    // We eliminate the bounces, and we get the state of the pins in flagBtnPins
    // as well as those whose state has been changed in &flagBtnChanged
    debounce(m_flag_ButtonsState, &m_flag_changedButtons);

    if (m_flag_changedButtons != 0) // if there is a change We write the changes in the event group
    {
      xEventGroupClearBits(evt_buttonsState, 0xFFFFFF);
      xEventGroupClearBits(evt_buttonsToggled, 0xFFFFFF);
      // We write the state of the buttons
      xEventGroupSetBits(evt_buttonsState, m_flag_ButtonsState);
      xEventGroupSetBits(evt_buttonsToggled, m_flag_changedButtons);
    }
    //  Serial.println(uxTaskGetStackHighWaterMark(NULL));
    vTaskDelay(m_taskDelay / portTICK_PERIOD_MS); // We come back to it every m_taskDelay ms
  }                                               // fin While
  vTaskDelete(NULL);
}

///////////////////////////////////////////////////////// UpdateButtons /////////////////////////////////////////////////////////////////////

Update_PushButtons::Update_PushButtons(uint16_t stackDepth, UBaseType_t priority, const char *name,
                             Debounced_PushButton *tab_buttons[], uint8_t nb_buttons,
                             EventGroupHandle_t eventToWaitFor, EventGroupHandle_t eventButtonsState) : Thread{stackDepth, priority, name}
{
  m_evt_ButtonsToggled = eventToWaitFor;
  m_evt_ButtonsState = eventButtonsState;
  m_nb_buttons = nb_buttons;
  m_tab_buttons = tab_buttons;
}

void Update_PushButtons::Main()
{
  while (true)
  {
    uint32_t flagsToggled = xEventGroupWaitBits(m_evt_ButtonsToggled, 0xFFFFFF, pdTRUE, pdFALSE, portMAX_DELAY);

    // We search for the modified buttons in m_evt_ButtonsToggled
    // We review the bits of flagsToggled if 1 then the button has been modified
    uint32_t flagsState = xEventGroupGetBits(m_evt_ButtonsState);

    for (uint8_t i = 0; i < m_nb_buttons; i++)
    {
      if (bitRead(flagsToggled, i) != 0)
      {
        // The button has been modified, we update its state
        m_tab_buttons[i]->setLastChangeTime(millis());
        if ((bitRead(flagsState, i)) == 0)
        {
          m_tab_buttons[i]->setState(DOWN);
#if ALLOW_PUSHBUTTONS_RELEASED_EVENT == 1
          esp_event_post(PUSHBUTTONS_EVENT, PUSHBUTTONS_EVENT_RELEASED, NULL, 0, portMAX_DELAY);
#endif
#if ALLOW_PUSHBUTTONS_CHANGED_EVENT == 1
          esp_event_post(PUSHBUTTONS_EVENT, PUSHBUTTONS_EVENT_CHANGED, NULL, 0, portMAX_DELAY);
#endif
#if ALLOW_PUSHBUTTONS_RELEASED_BUTTON_EVENT == 1
          esp_event_post(PUSHBUTTONS_EVENT, PUSHBUTTONS_EVENT_RELEASED_BUTTON, m_tab_buttons[i], 4, portMAX_DELAY);
#endif
#if ALLOW_PUSHBUTTONS_CHANGED_BUTTON_EVENT == 1
          esp_event_post(PUSHBUTTONS_EVENT, PUSHBUTTONS_EVENT_CHANGED_BUTTON, m_tab_buttons[i], 4, portMAX_DELAY);
#endif

        }
        else
        {
          m_tab_buttons[i]->setState(UP);
#if ALLOW_PUSHBUTTONS_PRESSED_EVENT == 1
          esp_event_post(PUSHBUTTONS_EVENT, PUSHBUTTONS_EVENT_PRESSED, NULL, 0, portMAX_DELAY);
#endif
#if ALLOW_PUSHBUTTONS_CHANGED_EVENT == 1
          esp_event_post(PUSHBUTTONS_EVENT, PUSHBUTTONS_EVENT_CHANGED, NULL, 0, portMAX_DELAY);
#endif
#if ALLOW_PUSHBUTTONS_PRESSED_BUTTON_EVENT == 1
          esp_event_post(PUSHBUTTONS_EVENT, PUSHBUTTONS_EVENT_PRESSED_BUTTON, m_tab_buttons[i], 4, portMAX_DELAY);
#endif
#if ALLOW_PUSHBUTTONS_CHANGED_BUTTON_EVENT == 1
          esp_event_post(PUSHBUTTONS_EVENT, PUSHBUTTONS_EVENT_CHANGED_BUTTON, m_tab_buttons[i], 4, portMAX_DELAY);
#endif

        }
      }
    }
    // Serial.println(uxTaskGetStackHighWaterMark(NULL));
  }
  vTaskDelete(NULL);
}

#endif //SCANPUSHBUTTONS_H