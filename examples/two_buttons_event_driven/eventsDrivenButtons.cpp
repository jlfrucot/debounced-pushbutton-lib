/**
 * @file main.cpp
 * @author Jean-Louis FRUCOT (frucot.jeanlouis@free.fr)
 * @brief two buttons event driven example
 * @version 1.0
 * @date 2022-03-08
 * @copyright GPL-3
 *
 * @note This example demonstrates the use of events related to DebouncedPushButtons.
 * First, we must modify platformio.ini in order to allow the compilation of the code managing the PUSHBUTTONS_EVENT
 *    ;###############################################################
 *    ; Debounced PushButtons library settings here (no need to edit library files):
 *    ;###############################################################
 *    build_flags =
 *     ; Allow the use of events if "=1" or prevent it if "=0"
 *     -D USE_PUSHBUTTONS_EVENTS=1
 *     ; Select active events (active "=1"(default), inactive "=0")
 *     -D ALLOW_PUSHBUTTONS_PRESSED_EVENT=0
 *     -D ALLOW_PUSHBUTTONS_RELEASED_EVENT=0
 *     -D ALLOW_PUSHBUTTONS_CHANGED_EVENT=0
 *     -D ALLOW_PUSHBUTTONS_CHANGED_BUTTON_EVENT=1
 *     -D ALLOW_PUSHBUTTONS_PRESSED_BUTTON_EVENT=1
 *     -D ALLOW_PUSHBUTTONS_RELEASED_BUTTON_EVENT=1
 *    ;#################################################################
 * If USE_PUSHBUTTONS_EVENTS is not set, events are considered disabled.
 * If ALLOW_PUSHBUTTONS_XXXXX_EVENT is not set, it is considered active.
 * 
 * After including the #include "debouncedPushButtons.h" library,
 * we define the family of events:
 *   ESP_EVENT_DEFINE_BASE(PUSHBUTTONS_EVENT);
 * Then we create the debouncedPushButtons and the button scanner.
 * Next, it is necessary to define the event handlers which will react to one of the 6 implemented events
 *
 *   PUSHBUTTONS_EVENT_PRESSED,        // raised when a button is pressed
 *   PUSHBUTTONS_EVENT_RELEASED,       // raised when a button is released
 *   PUSHBUTTONS_EVENT_CHANGED,        // raised when a button has changed
 *   PUSHBUTTONS_EVENT_CHANGED_BUTTON, // raised when a button has changed
 *   PUSHBUTTONS_EVENT_PRESSED_BUTTON, // raised when a button is pressed
 *   PUSHBUTTONS_EVENT_RELEASED_BUTTON // raised when a button is released
 *
 * The first three are issued regardless of the button that generated it,
 * the next three report the button pointer in the variable void * event_data
 * Event handlers have the following signature:
 *   void event_handler_1(void *handler_arg, esp_event_base_t base, int32_t id, void *)
 * or
 *   void event_handler_2(void *handler_arg, esp_event_base_t base, int32_t id, void * event_data)
 * If you want to use event_data, it is necessary to do a static_cast in order to give it back its type:
 *   Debounced_PushButton *button = static_cast<Debounced_PushButton *>(event_data);
 *
 * Then in setup(), we add the buttons to the scanner and we start it:
 *   scanner->addPushButton(button0);   // We add the buttons to the scanner
 *   scanner->addPushButton(button1);   // If there are more, a loop may be more efficient
 *   scanner->startUpdatePushButtons(); // Starts the task that updates the state of the buttons
 *
 * We then launch the event management:
 *   initPushButtonEvent();
 * And we register the event handlers:
 *   esp_event_handler_register(PUSHBUTTONS_EVENT, PUSHBUTTONS_EVENT_CHANGED_BUTTON, event_handler_2, event_data);
 *
 * And that's all !
 */
#include <Arduino.h>
#include "debouncedPushButtons.h"
#include "scanPushButtons.h"
ESP_EVENT_DEFINE_BASE(PUSHBUTTONS_EVENT); // Allows the use of PUSHBUTTON_EVENT

#define pin_1 25 // pin to which the first button is attached
#define pin_2 18 // pin to which the second button is attached

Debounced_PushButton *button0 = new Debounced_PushButton(pin_1, FALLING_COUNT, INPUT_PULLUP);
Debounced_PushButton *button1 = new Debounced_PushButton(pin_2, INPUT_PULLUP);

Scan_PushButtons *scanner = new Scan_PushButtons{1000, 3, "scanner"};

bool buttonPressed = false;
bool buttonReleased = false;
bool buttonChanged = false;

///////////////////////////////// PUSHBUTTONS_EVENT event Handlers /////////////////////////////////////////

// void run_on_pressed_event(void *handler_arg, esp_event_base_t base, int32_t id, void *)
// {
//   buttonPressed = true;
// }

// void run_on_released_event(void *handler_arg, esp_event_base_t base, int32_t id, void *)
// {
//   buttonReleased = true;
// }

// void run_on_changed_event(void *handler_arg, esp_event_base_t base, int32_t id, void *)
// {
//   buttonReleased = true;
// }

// void run_on_pressedButton_event(void *handler_arg, esp_event_base_t base, int32_t id, void *event_data)
// {
//   Debounced_PushButton *button = static_cast<Debounced_PushButton *>(event_data);
//   Serial.println(button->getState());
// }

// void run_on_releasedButton_event(void *handler_arg, esp_event_base_t base, int32_t id, void *event_data)
// {
//   Debounced_PushButton *button = static_cast<Debounced_PushButton *>(event_data);
//   Serial.println(button->getState());
// }

void run_on_changedButton_event(void *handler_arg, esp_event_base_t base, int32_t id, void *event_data)
{
  Debounced_PushButton *button = static_cast<Debounced_PushButton *>(event_data);
  uint8_t pin = button->getPin();
  switch (pin)
  {
  case pin_1:
    Serial.print("button attached at pin N°");
    Serial.print(pin);
    Serial.print(" Status : ");
    Serial.println(button->getState());
    break;
  case pin_2:
    Serial.print("button attached at pin N°");
    Serial.print(pin);
    Serial.print(" Status : ");
    Serial.println(button->getState());
    break;
  default: // Normally, nothing to do here !
    break;
  }
}
///////////////////////////////// End  PUSHBUTTONS_EVENT event Handlers //////////////////////////////////////

void setup()
{
  Serial.begin(115200);
  delay(500);
  scanner->addPushButton(button0);   // We add the buttons to the scanner
  scanner->addPushButton(button1);   // If there are more, a loop may be more efficient
  scanner->startUpdatePushButtons(); // Starts the task that updates the state of the buttons
#if USE_PUSHBUTTONS_EVENTS == 1
  initPushButtonEvent(); // Init PushButtons Events management
  // esp_event_handler_register(PUSHBUTTONS_EVENT, PUSHBUTTONS_EVENT_PRESSED, run_on_pressed_event, nullptr);
  // esp_event_handler_register(PUSHBUTTONS_EVENT, PUSHBUTTONS_EVENT_RELEASED, run_on_released_event, nullptr);
  // esp_event_handler_register(PUSHBUTTONS_EVENT, PUSHBUTTONS_EVENT_CHANGED, run_on_changed_event, nullptr);

  // esp_event_handler_register(PUSHBUTTONS_EVENT, PUSHBUTTONS_EVENT_PRESSED_BUTTON, run_on_pressedButton_event, event_data);
  // esp_event_handler_register(PUSHBUTTONS_EVENT, PUSHBUTTONS_EVENT_RELEASED_BUTTON, run_on_releasedButton_event, event_data);
  esp_event_handler_register(PUSHBUTTONS_EVENT, PUSHBUTTONS_EVENT_CHANGED_BUTTON, run_on_changedButton_event, event_data);
#endif
}
void loop()
{
}
