/**
 * @file main.cpp
 * @author Jean-Louis FRUCOT (frucot.jeanlouis@free.fr)
 * @brief Simple two buttons example
 * @version 0.1
 * @date 2022-02-20
 * @copyright GPL-3
 * 
 * 
 */
#include <Arduino.h>
#include "debouncedPushButtons.h"
#include "scanPushButtons.h"

#define pin_1 25 // pin to which the first button is attached
#define pin_2 18 // pin to which the second button is attached

Debounced_PushButton *button0 = new Debounced_PushButton(pin_1, INPUT_PULLUP);
Debounced_PushButton *button1 = new Debounced_PushButton(pin_2, INPUT_PULLUP);
Scan_PushButtons *scanner = new Scan_PushButtons{1000, 3, "scanner"};
int n = 0;
Debounced_PushButton * tab_buttons[2] = {button0, button1};
void setup()
{
  Serial.begin(115200);
  delay(500);
  scanner->addPushButton(button0);   // We add the buttons to the scanner
  scanner->addPushButton(button1);   // If there are more, a loop may be more efficient
  scanner->startUpdatePushButtons(); // Starts the task that updates the state of the buttons
}

void loop()
{
// Display the state of registered buttons (just for demonstration !)

  for (uint8_t i = 0; i < 2; i++)
  {
    Serial.print("Button N° ");
    Serial.print(i);
    Serial.print(" Pin : ");
    Serial.print(tab_buttons[i]->getPin());
    Serial.print(" Just changed ? ");
    if (tab_buttons[i]->hasJustChanged() == true)
    {
      Serial.print(" Yes");
    }
    else
    {
      Serial.print(" No");
    }
    Serial.print(" When ? ");
    Serial.print(tab_buttons[i]->getLastChangeTime());
    Serial.print(" Status : ");
    if (tab_buttons[i]->getState() == UP)
    {
      Serial.print("UP");
    }
    else
    {
      Serial.print("DOWN");
    }
    Serial.println(" ");
  }

  delay(1000);
}