# Debounced PushButton Lib

Here is a library allowing to manage pushButtons (up to 24) with an ESP32. The goal is to minimize the size of the code needed by using the concept of vertical counter to process the mechanical bounces of the pushButtons in parallel. We implement two tasks managing these buttons: one scans the pins to which the buttons are attached, the other updates, in case of change, the status of these buttons.
In addition, events have been attached to state changes, which optimizes the code and its memory footprint.
The use of events can be configured in platformio.ini, this allows to deactivate the use of events, and/or to activate only some of them, and thus to optimize the size of the generated code.

## Utilisation basic


After including the library, it is necessary to add this line wich allows events (even not use)

~~~c
    ESP_EVENT_DEFINE_BASE(PUSHBUTTONS_EVENT); // Allows the use of PUSHBUTTON_EVENT  
~~~

First, it is necessary to declare the buttons to manage:

~~~c
DebouncedPushButton *button0 = new DebouncedPushButton(pin_0, INPUT_PULLUP);
DebouncedPushButton *button1 = new DebouncedPushButton(pin_1, INPUT_PULLUP);

~~~

Then, we create a "scanner" of buttons:
~~~c
scanPushButtons *scanner = new scanPushButtons{1000, 3, "scanner"};
~~~
Then, in the setup() part, we add the buttons to manage to the scanner and we start the scanner:
~~~c
scanner->addPushButton(button0);   // We add the buttons to the scanner
scanner->addPushButton(button1);   // If there are more, a loop may be more efficient
scanner->startUpdatePushButtons(); // Starts the task that updates the state of the buttons
~~~

Now, in loop(), it is possible to test the buttons:

~~~c
Serial.print(" Just changed ? ");
    if (button0->hasJustChanged() == true)
    {
      Serial.print(" Yes");
    }
    else
    {
      Serial.print(" No");
    }
    Serial.print(" When ? ");
    Serial.print(button0->getLastChangeTime());
    Serial.print(" Status : ");
    if (button0->getState() == UP)
    {
      Serial.print("UP");
    }
    else
    {
      Serial.print("DOWN");
    }
~~~

## Events use

The use of events allows a better reactivity of the code, and facilitates the writing of the code.
Six events have been implemented: three signaling the fact that a button has been pressed, released, or modified, the other three signal the same events by returning the button at the origin of the change.
~~~c
enum
{                                     // declaration of the specific events under the PUSHBUTTONS event family
    PUSHBUTTONS_EVENT_PRESSED,        // raised when a button is pressed
    PUSHBUTTONS_EVENT_RELEASED,       // raised when a button is released
    PUSHBUTTONS_EVENT_CHANGED,        // raised when a button has changed
    PUSHBUTTONS_EVENT_CHANGED_BUTTON, // raised when a button has changed, return *pushButton
    PUSHBUTTONS_EVENT_PRESSED_BUTTON, // raised when a button is pressed, return *pushButton
    PUSHBUTTONS_EVENT_RELEASED_BUTTON // raised when a button is released, return *pushButton
};
~~~
To use these features, you must respect the following scheme:

Premièrement, il faut modifier le fichier platformio.ini comme suit :

~~~
;###############################################################
; Debounced PushButtons library settings here (no need to edit library files):
;###############################################################
build_flags =
; Allow the use of events if "=1" or prevent it if "=0"
  -D USE_PUSHBUTTONS_EVENTS=1
; Select active events (active "=1"(default), inactive "=0")
  -D ALLOW_PUSHBUTTONS_PRESSED_EVENT=0
  -D ALLOW_PUSHBUTTONS_RELEASED_EVENT=0
  -D ALLOW_PUSHBUTTONS_CHANGED_EVENT=0
  -D ALLOW_PUSHBUTTONS_CHANGED_BUTTON_EVENT=1
  -D ALLOW_PUSHBUTTONS_PRESSED_BUTTON_EVENT=1
  -D ALLOW_PUSHBUTTONS_RELEASED_BUTTON_EVENT=1

~~~
If USE_PUSHBUTTONS_EVENT is not defined, events are considered as disabled.
If an event ALLOW_PUSHBUTTONS_XXXXXXX_EVENT is not defined, it is considered as enabled.

Then in main.cpp, eclare the use of this family of events:
~~~c
    ESP_EVENT_DEFINE_BASE(PUSHBUTTONS_EVENT); // Allows the use of PUSHBUTTON_EVENT  
~~~
Declare and define event handlers:
~~~c
void run_on_pressed_event(void *handler_arg, esp_event_base_t base, int32_t id, void *)
 {
   // personnal code
 }
~~~
or
~~~c
 void run_on_changedButton_event(void *handler_arg, esp_event_base_t base, int32_t id, void *event_data)
{
  DebouncedPushButton *button = static_cast<DebouncedPushButton *>(event_data); // pointer to fired button

}
It is necessary to perform a static_cast to obtain an object of type DeboucedPushButton *.
   ~~    
Then in setup(), launch the event loop:
~~~c
  initPushButtonEvent();                                   // Init PushButtons Events management
~~~
Then register the event handlers:
~~~c
  esp_event_handler_register(PUSHBUTTONS_EVENT, PUSHBUTTONS_EVENT_CHANGED_BUTTON, run_on_changedButton_event, event_data);

~~~

## Available Functions

### Class DebouncedPushButtons

- setCountMode(enum_CountMode mode) 
Where mode could be one below
    NO_COUNT,   // We don't count anything
    RISING_COUNT,  // We count the rising edges
    FALLING_COUNT, // We count the falling edges
    BOTH_COUNT     // We count everything
- getState() //Return button state
- getPin() // Return pin num
- getLastChangeTime() // return time when button has changed
- getCount()  // Return the number of times the button changed, state based on countMode
- isPressed() 
- hasJustChanged() // After use return false
- reset_Count() // !!!

### Class scanPushButtons

- addPushButton(DebouncedPushButton *button) // add a *button to the scanner
- DebouncedPushButton **getPushButtonsArray() // return the tab of managed buttons
- uint8_t getPushButtonsArraySize(); // how many buttons are managed
- void stop(); // stop the scanning of buttons
- void restart(); // restart the scanning of buttons
- void setTaskDelay(uint16_t delay); // set the delay between scans

Available events :

Family event is PUSHBUTTONS_EVENT
enum
{                                     // declaration of the specific events under the PUSHBUTTONS event family
    PUSHBUTTONS_EVENT_PRESSED,        // raised when a button is pressed
    PUSHBUTTONS_EVENT_RELEASED,       // raised when a button is released
    PUSHBUTTONS_EVENT_CHANGED,        // raised when a button has changed
    PUSHBUTTONS_EVENT_CHANGED_BUTTON, // raised when a button has changed
    PUSHBUTTONS_EVENT_PRESSED_BUTTON, // raised when a button is pressed
    PUSHBUTTONS_EVENT_RELEASED_BUTTON // raised when a button is released
};