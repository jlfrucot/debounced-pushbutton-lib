#ifndef OO_TASK
#define OO_TASK

/*Copyright (C) 
 * 2018 - fjrg76 at gmail dot com
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 * 
 */



#if defined (__USE_LPCOPEN)
#if defined(NO_BOARD_LIB)
#include "chip.h"
#else
#include "board.h"
#endif
#endif

// #include <cr_section_macros.h>

#include <FreeRTOS.h>
#include <esp_task.h>
#include <Arduino.h>


class Thread
{
public:
	Thread( uint16_t _stackDepth, UBaseType_t _priority, const char* _name = "" )
	{
		xTaskCreate( task, _name, _stackDepth, this, _priority, &this->taskHandle );
	}

	virtual void Main() = 0;

protected:
	static void task( void* _params )
	{
		Thread* p = static_cast<Thread*>( _params );
		p->Main();
	}

	TaskHandle_t taskHandle;
};

#endif  // OO_TASK
//------------------------------------------------------------------------------
