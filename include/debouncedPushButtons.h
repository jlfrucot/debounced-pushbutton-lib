#ifndef DEBOUNCEDPUSHBUTTONS_H
#define DEBOUNCEDPUSHBUTTONS_H
/* jlf 20220110 */

#include <Arduino.h>
// // #include <Arduino.h>
// #include <esp_event.h>
// // #include "debouncedPushButtons.h"
// #include "settings.h"
// #include "OO_Task.h"
/** Enumeration of the possible states of the button **/
enum enum_State
{
    DOWN_UP, // Rising edge
    UP_DOWN, // Falling Edge
    UP,      // Released
    DOWN,    // Pressed
    NONE     // Unknown
};

/** Enumeration defining counting modes: nothing, rising edge, falling edge, all **/
enum enum_CountMode
{
    NO_COUNT,      // We don't count anything
    RISING_COUNT,  // We count the rising edges
    FALLING_COUNT, // We count the falling edges
    BOTH_COUNT     // We count everything
};
/**  @class Class used to describe a button
 * with as parameter the number of the pin to which it is connected, a click counter, and the input mode
 * and allowing access to the state of the button
 **/
class Debounced_PushButton
{

public:
    // /** Constructeur du bouton, le input mode est INPUT_PULLUP
    //     @param pin le numéro du pin auquel est rattaché le bouton
    // **/
    // Debounced_PushButton(uint8_t pin);

    /** Button constructor, input mode is INPUT_PULLUP
        @param pin the number of the pin to which the button is attached
        @param inputMode to choose between INPUT_PULLUP, INPUT_PULLDOWN, INPUT
    **/
    Debounced_PushButton(uint8_t pin, uint8_t inputMode);

    // /** Constructeur du bouton
    //     @param pin le numéro du pin auquel est rattaché le bouton
    //     @param mode la façon dont les clics sont comptés : NO_COUNT, RISING_COUNT, FALLING_COUNT, BOTH_COUNT
    // **/
    // Debounced_PushButton(uint8_t pin, enum_CountMode mode);

    /** button constructor
        @param pin the number of the pin to which the button is attached
        @param mode how clicks are counted: NO_COUNT, RISING_COUNT, FALLING_COUNT, BOTH_COUNT
        @param inputMode to choose between INPUT_PULLUP, INPUT_PULLDOWN, INPUT
    **/
    Debounced_PushButton(uint8_t pin, enum_CountMode mode, uint8_t inputMode);

    /** Destructeur **/
    ~Debounced_PushButton();

    /** Returns the state of the button
        @return button state: see enum_State enumeration
    **/
    enum_State getState();

    /** Sets how button clicks are counted
     * @param mode to choose between NO_COUNT, RISING_COUNT, FALLING_COUNT, BOTH_COUNT
     **/
    void setCountMode(enum_CountMode mode);

    /** Defines the state of the button, in principle, we do not use this function because the state is updated by the UpdateButtons task
     * @param state the state of the UP or DOWN button, optionally NONE
     **/
    void setState(enum_State state);

    /** Is the button pressed?
     * @return true if pressed false otherwise
     **/
    bool isPressed();

    /** Is the button released?
     * @return true if released false otherwise
     **/
    bool isReleased();

    /** Return the number of the pin to which the button is attached (in case we forgot it)
     * @return the pin number
     **/
    uint8_t getPin();

    /** Did the button just change state? This value changes to false following a call
     * @return true if there was a change of state, false otherwise
     **/
    bool hasJustChanged();

    /** When did the button change state?
     * @return the change time in milliseconds
     **/
    uint32_t getLastChangeTime();

    /** returns the number of state changes
     * @return the number of state changes, depending on the counting mode
     * @see Debounced_PushButton::setCountMode()
     **/
    uint32_t getCount();

    /** increments the number of state changes
     **/
    void incCount();

    /** As indicated by his name ! **/
    void resetCount();

    /** Defines when the button changed state
     * @param time the moment of the state change, in milliseconds.
     * @note Normally, this function should'nt be use !
     **/
    void setLastChangeTime(uint32_t time); // milliseconds

private:
    uint8_t m_flag_State = 0;      // Button state flags
    uint8_t m_pin;                 // the pin to which the button is attached
    uint32_t m_lastChangeTime = 0; // The time of the last change in ms
    uint32_t m_count = 0;          // The number of state changes depending on the counting mode
};

#endif // DEBOUNCEDPUSHBUTTONS_H
