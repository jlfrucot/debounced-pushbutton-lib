// Events not defined in platformio.ini are considered active by default.
#ifndef ALLOW_PUSHBUTTONS_PRESSED_EVENT
#define ALLOW_PUSHBUTTONS_PRESSED_EVENT 1
#endif
#ifndef ALLOW_PUSHBUTTONS_RELEASED_EVENT
#define ALLOW_PUSHBUTTONS_RELEASED_EVENT 1
#endif
#ifndef ALLOW_PUSHBUTTONS_CHANGED_EVENT
#define ALLOW_PUSHBUTTONS_CHANGED_EVENT 1 
#endif
#ifndef ALLOW_PUSHBUTTONS_CHANGED_BUTTON_EVENT
#define ALLOW_PUSHBUTTONS_CHANGED_BUTTON_EVENT  1
#endif
#ifndef ALLOW_PUSHBUTTONS_PRESSED_BUTTON_EVENT
#define ALLOW_PUSHBUTTONS_PRESSED_BUTTON_EVENT 1
#endif
#ifndef ALLOW_PUSHBUTTONS_RELEASED_BUTTON_EVENT
#define ALLOW_PUSHBUTTONS_RELEASED_BUTTON_EVENT 1
#endif

//If USE_PUSHBUTTONS_EVENTS is not set, event handling is disabled and various events are disabled, 
//even if they are enabled in platform.ini.
#ifndef USE_PUSHBUTTONS_EVENTS
    #define USE_PUSHBUTTONS_EVENTS 0
    #undef ALLOW_PUSHBUTTONS_PRESSED_EVENT
    #define ALLOW_PUSHBUTTONS_PRESSED_EVENT 0
    #undef ALLOW_PUSHBUTTONS_RELEASED_EVENT
    #define ALLOW_PUSHBUTTONS_RELEASED_EVENT 0
    #undef ALLOW_PUSHBUTTONS_CHANGED_EVENT
    #define ALLOW_PUSHBUTTONS_CHANGED_EVENT 0
    #undef ALLOW_PUSHBUTTONS_CHANGED_BUTTON_EVENT
    #define ALLOW_PUSHBUTTONS_CHANGED_BUTTON_EVENT 0
    #undef ALLOW_PUSHBUTTONS_PRESSED_BUTTON_EVENT
    #define ALLOW_PUSHBUTTONS_PRESSED_BUTTON_EVENT 0
    #undef ALLOW_PUSHBUTTONS_RELEASED_BUTTON_EVENT
    #define ALLOW_PUSHBUTTONS_RELEASED_BUTTON_EVENT 0
//If USE_PUSHBUTTONS_EVENTS is set to 0, event handling is disabled and various events are disabled, 
//even if they are enabled in platform.ini.
#elif USE_PUSHBUTTONS_EVENTS == 0
    #undef ALLOW_PUSHBUTTONS_PRESSED_EVENT
    #define ALLOW_PUSHBUTTONS_PRESSED_EVENT 0
    #undef ALLOW_PUSHBUTTONS_RELEASED_EVENT
    #define ALLOW_PUSHBUTTONS_RELEASED_EVENT 0
    #undef ALLOW_PUSHBUTTONS_CHANGED_EVENT
    #define ALLOW_PUSHBUTTONS_CHANGED_EVENT 0
    #undef ALLOW_PUSHBUTTONS_CHANGED_BUTTON_EVENT
    #define ALLOW_PUSHBUTTONS_CHANGED_BUTTON_EVENT 0
    #undef ALLOW_PUSHBUTTONS_PRESSED_BUTTON_EVENT
    #define ALLOW_PUSHBUTTONS_PRESSED_BUTTON_EVENT 0
    #undef ALLOW_PUSHBUTTONS_RELEASED_BUTTON_EVENT
    #define ALLOW_PUSHBUTTONS_RELEASED_BUTTON_EVENT 0
#endif