#include <Arduino.h>
#include <esp_event.h>
#include "debouncedPushButtons.h"
#include "settings.h"
#include "OO_Task.h"

#ifndef SCANNPUSHBUTTON_H
#define SCANNPUSHBUTTON_H



#if USE_PUSHBUTTONS_EVENTS == 1
ESP_EVENT_DECLARE_BASE(PUSHBUTTONS_EVENT); // declaration of the pushButtons events family

enum
{                                     // declaration of the specific events under the PUSHBUTTONS event family
    PUSHBUTTONS_EVENT_PRESSED,        // raised when a button is pressed
    PUSHBUTTONS_EVENT_RELEASED,       // raised when a button is released
    PUSHBUTTONS_EVENT_CHANGED,        // raised when a button has changed
    PUSHBUTTONS_EVENT_CHANGED_BUTTON, // raised when a button has changed
    PUSHBUTTONS_EVENT_PRESSED_BUTTON, // raised when a button is pressed
    PUSHBUTTONS_EVENT_RELEASED_BUTTON // raised when a button is released
};

static void *event_data = nullptr;
static void initPushButtonEvent()
{
    esp_event_loop_create_default();
}
#endif

#define bit_SomethingChanged (1U << 23)

// bits utilisés par m_flag_State : état du bouton
#define bit_state        // 0 DOWN, 1 UP
#define bit_JustChanged  // 0 no change, 1 recent change
#define bit_RisingCount  // The counter counts the rising changes
#define bit_FallingCount // The counter counts the falling changes

///////////////////////////////////////////////////////////////////////////////////////////
// /** @class Update_PushButtons inheriting from class Thread which updates data of a given button
//  *
//  **/
class Update_PushButtons : public Thread
{
public:
    Update_PushButtons(uint16_t stackDepth, UBaseType_t priority, const char *name,
                       Debounced_PushButton *tab_buttons[], uint8_t nb_buttons,
                       EventGroupHandle_t eventToWaitFor, EventGroupHandle_t eventButtonsState);
    ~Update_PushButtons(){};
    virtual void Main() override;

private:
    uint8_t m_nb_buttons = 0;
    Debounced_PushButton **m_tab_buttons = NULL;
    uint16_t m_flag_ButtonsState;            // flag representing the state of monitored buttons
    uint16_t m_flag_changedButtons;          // flag representing the modified buttons
    EventGroupHandle_t m_evt_ButtonsToggled; // event group listing changed buttons
    EventGroupHandle_t m_evt_ButtonsState;   // event group listing button state
};

/** @class Class instantiating two tasks,
one testing button changes with a debounce procedure using a vertical counter allowing simultaneous processing of 24 buttons,
the other which updates the state of the buttons if necessary.

* @see https://fjrg76.wordpress.com/2018/05/20/objectifying-task-creation-in-freertos/
**/
class Scan_PushButtons : public Thread
{

public:
    /** Instantiates a button scanner
        @param stackDepth the size of the task's data stack
        @param priority the priority of the task: the higher the number, the higher the priority
        @param name the human-readable name of the task
    **/
    Scan_PushButtons(uint16_t stackDepth, UBaseType_t priority, const char *name);
    ~Scan_PushButtons();
    /** function implementing the task code **/
    virtual void Main() override;
    /** Adds a button to the set of managed buttons (limited to 24 or 8 buttons)
     *   @param button pointer to button instance
     **/
    void addPushButton(Debounced_PushButton *button);

    /**
     * @brief Get the pointer to Push Buttons Array object
     *
     * @return Debounced_PushButton*
     */
    Debounced_PushButton **getPushButtonsArray();

    /**
     * @brief Get the Push Buttons Array Size object
     *
     * @return uint8_t
     */
    uint8_t getPushButtonsArraySize();

    /** Starts the referenced buttons scanner **/
    void startUpdatePushButtons();

    /**
     * @brief Suspend buttons scan task
     */
    void stop();

    /**
     * @brief Restart buttons scan task
     */
    void restart();

    /** Determines the scanning delay (not guaranteed)
     * @param delay in milliseconds the value of 5 ms works well, but can be increased depending on other needs
     **/
    void setTaskDelay(uint16_t delay);

    EventGroupHandle_t evt_buttonsState; // Event group representing the state of the buttons

    EventGroupHandle_t evt_buttonsToggled; // Event group referencing changed buttons
private:
    /** "Debouncing" function using the concept of "vertical counter" allowing to debounce 24 buttons simultaneously
     * @see https://www.compuphase.com/electronics/debouncing.htm
     * @param sample flag containing the state of the HIGH(1)/LOW(0) buttons
     * @param toggle flag containing the status of debounced buttons: modified (1) or not (0)
     * @return button status
     **/
    inline uint32_t debounce(uint32_t sample, uint32_t *toggle);

#if configUSE_16_BIT_TICKS == 0
    static const uint8_t m_nb_max = 24; // max number of buttons 24 or 8
#else
    static const uint8_t m_nb_max = 8;
#endif
    Debounced_PushButton *m_tab_pushButtons[m_nb_max]; // array of buttons referenced in this object
    uint8_t m_nb_registeredButtons = 0;                // number of monitored buttons
    uint32_t m_flag_ButtonsState;                      // flag representing the state of monitored buttons
    uint32_t m_flag_changedButtons;                    // flag representing the modified buttons
    uint16_t m_taskDelay = 10;                         // delay during which the task is blocked: allows multitasking
};
#endif //SCANNPUSHBUTTON_H